package com.mitocode;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

		public static final Contact DEFAULT_CONTACT = new Contact("nombre empresa","sitioWeb.com","correo@empresa.com");
	
		public static final ApiInfo DEFAULT_INFO_API = new ApiInfo("Mediaapp api documentation","mediapp documentation api","1.0",
				"PREMIUM",DEFAULT_CONTACT,"Apache 2.0","licencia apahce",new ArrayList<VendorExtension>());
		@Bean
		public Docket api() {
			return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_INFO_API);
		}
}
