package com.mitocode.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.model.Examen;
import com.mitocode.model.Medico;
import com.mitocode.model.Paciente;
import com.mitocode.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private IMedicoService service;
	
	
	//DINAMICO CON EL ALGORITMO
	//@PreAuthorize("@restAuthService.hasAccess('listar')")
	@GetMapping(produces = "application/json")
	public List<Medico> listar(){
		return service.listar();
	}
	
	
	
	//HARD CODE
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@GetMapping(value = "/{id}", produces = "application/json")
	public Medico listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json",consumes = "application/json")
	public ResponseEntity<Object> registrar(@Valid @RequestBody Medico med) {
		Medico medico = new Medico();
		medico = service.registrar(med);
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(medico.getIdMedico()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping (produces = "application/json",consumes = "application/json")
	public ResponseEntity<Object> modificar(@Valid @RequestBody Medico med) {
		service.modificar(med);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id){
		Medico med = service.listarId(id); 	
		if(med == null) {
			throw new ModelNotFoundException("ID NO ENCONTRADO:"+id);
		}else {
			 service.eliminar(id);
		}
	
	}
}
