package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IEspecialidadDAO;
import com.mitocode.model.Especialidad;
import com.mitocode.model.Examen;
import com.mitocode.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService{

	@Autowired
	private IEspecialidadDAO dao;
	
	@Override
	public Especialidad registrar(Especialidad t) {	
		return dao.save(t);
	}

	@Override
	public Especialidad modificar(Especialidad t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		//dao.delete(id);
		dao.deleteById(id);
	}

	@Override
	public Especialidad listarId(int id) {
		//return dao.findOne(id);
		//INI-CAMBIO PARA SPRING BOOT 2
		Optional<Especialidad> opt = dao.findById(id);
		return opt.isPresent() ? opt.get() : new Especialidad();
	}

	@Override
	public List<Especialidad> listar() {
		return dao.findAll();
	}

}
