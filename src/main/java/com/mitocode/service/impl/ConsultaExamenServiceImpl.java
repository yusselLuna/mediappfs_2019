package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IConsultaExamenDAO;
import com.mitocode.model.ConsultaExamen;
import com.mitocode.model.Examen;
import com.mitocode.service.IConsultaExamenService;

@Service
public class ConsultaExamenServiceImpl implements IConsultaExamenService{

	@Autowired
	private IConsultaExamenDAO dao;

	@Override
	public ConsultaExamen registrar(ConsultaExamen t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public ConsultaExamen modificar(ConsultaExamen t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		//dao.delete(id);
		dao.deleteById(id);
		
	}

	@Override
	public ConsultaExamen listarId(int id) {
		// TODO Auto-generated method stub
		//return dao.findOne(id);
		//INI-CAMBIO PARA SPRING BOOT 2
		Optional<ConsultaExamen> opt = dao.findById(id);
		return opt.isPresent() ? opt.get() : new ConsultaExamen();
	}

	@Override
	public List<ConsultaExamen> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public List<ConsultaExamen> listarExamenesPorConsulta(Integer idconsulta) {
		// TODO Auto-generated method stub
		return dao.listarExamenesPorConsulta(idconsulta);
	}
	
	

}
