package stepDefinition;

import static org.junit.Assert.assertEquals;
import org.apache.log4j.Logger;
import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import java.util.List;



public class StepsPruebaCucumber {
	
	private final static Logger log = Logger.getLogger(StepsPruebaCucumber.class);
	
	private int start;
	private int left;
	private boolean status;
	
	@Given("Hay {int} cervezas")
	public void hay_cervezas(Integer start) {
	    this.start = start;
	    
	}

	@When("Tomo {int} cervezas")
	public void tomo_cervezas(Integer drink) {
	    this.left = this.start - drink;
	    
	}

	@Then("Deberian quedar {int} cervezas")
	public void deberian_quedar_cervezas(Integer left) {
		if(this.left==left)
			this.status=true;
		else
			this.status=false;
	        assertEquals(true, status);
	    
	}
	
	@Given("Un blog llamado {string} 	 con el siguiente contenido")
	public void un_blog_llamado_con_el_siguiente_contenido(String comillasDobles, String docString) {
		log.info(comillasDobles);
		log.info(docString);
		
	    
	}
	
	@Given("Los siguientes usuarios existentes:")
	public void los_siguientes_usuarios_existentes(DataTable dataTable) {
		List<List<String>> rows = dataTable.asList(String.class);
		//inicio es inclusivo
		//fin es exlusivo por eso no restamos -1
		List<List<String>> rowsWithoutHeader = rows.subList(1,rows.size());
		log.info("dataTAble: \n"+dataTable);

	}

}
