package runSteps;

import cucumber.api.CucumberOptions;
import static cucumber.api.SnippetType.CAMELCASE;

@CucumberOptions(
		
	    features = "src/test/resources/features",
	    glue = "stepDefinitions",
	    plugin = {"pretty","json:target/cucumber-reports/cucumber.jsons"},
	    snippets = CAMELCASE
	)
public class RunPruebaTest {

}
